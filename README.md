Overview

Code sample shows how to transfer data between two iOS devices, with one acting as a Bluetooth central and the other as a peripheral, by using a CBCharacteristic on the peripheral side that changes its value. The value change is automatically picked up on the central side.

This sample shows how to handle flow control in this scenario. It also covers a rudimentary way to use the Received Signal Strength Indicator (RSSI) value to determine whether data transfer is feasible.


Run the sample on two devices that support Bluetooth LE.

On one device, tap the “Central” button. This device will be the central mode device. The device will begin scanning for a peripheral device that is advertising the Transfer Service.

On the other device, tap the “Peripheral” button. This device will be the peripheral mode device.

On the peripheral mode device, tap the advertise on/off switch, to enable peripheral mode advertising of the data in the text field.

Bring the two devices close to each other.