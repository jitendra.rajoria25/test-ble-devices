//
//  AppDelegate.swift
//  testBLEDevices
//
//  Created by Jitendra Rajoria on 9/20/20.
//  Copyright © 2020 Jitendra Rajoria. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

